# Copyright (C) AGRA - University of Bremen
#
# LICENSE : Eclipse Public License (EPL 1.0)

project("Minikit" CXX)
cmake_minimum_required(VERSION 3.0)


include(utils/cmake/CCache.cmake)
include(utils/cmake/CompilerFlags.cmake)
include(utils/cmake/ListDir.cmake)
include(utils/cmake/CMakeProcedures.cmake)

set(CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/utils/cmake/modules/")

message ("TOP-DIR :: ${CMAKE_SOURCE_DIR}")
set(ext_BOOST ${CMAKE_SOURCE_DIR}/ext/boost)
include_directories(${ext_BOOST} ext/include ${abc_SRC})
add_subdirectory(ext)

set( CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/programs )

# Options
option(axekit_ENABLE_PROGRAMS "build programs" on)
option(axekit_BUILD_SHARED "build shared libraries" on)
set(axekit_PACKAGES "" CACHE STRING "if non-empty, then only the packages in the semicolon-separated lists are build")

# Libraries (include)
include_directories(src)
add_subdirectory(src)


