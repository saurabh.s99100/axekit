// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : helloWorld.hpp
//------------------------------------------------------------------------------
#pragma once
#ifndef HELLOWORLD_HPP
#define HELLOWORLD_HPP

#include <iostream>
#include <cstdlib>

#include <utils/program_options.hpp>
#include <utils/helloWorld_utils.hpp>

#include <ext-libs/abc/abc_api.hpp>
#include <utils/yosys_utils.hpp>

namespace axekit {
    
  
}


#endif

//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
