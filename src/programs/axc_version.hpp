// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : axc_version.hpp
//------------------------------------------------------------------------------
#pragma once
#ifndef AXC_VERSION_HPP
#define AXC_VERSION_HPP

#include<iostream>
#include<string>

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)
#ifndef GIT_COMMIT_HASH
#define GIT_COMMIT_HASH NA
#endif
#ifndef GIT_BRANCH
#define GIT_BRANCH NA
#endif

inline std::string banner_string () {
  std::string gtb  = std::string ( TOSTRING(GIT_BRANCH) );
  std::string gtc  = std::string ( TOSTRING(GIT_COMMIT_HASH) );
  
  //std::string line = "**********************************************************\n";
  //std::string uni  = " Group of Computer Architecture (AGRA) - Uni Bremen \n";
  //std::string url  = " (http://www.informatik.uni-bremen.de/agra/eng/index.php)\n";
  //std::string txt  = "AxC : Approximate Computing Toolkit (build " + gtb + "-" + gtc + ")\n" ;


  const static std::string a = "/*************************************************************\\ \n";
  const static std::string b = "*                                                             * \n";
  const static std::string c = "*                        aXc                                  * \n";
  const static std::string d = "*            approXimate computing toolkit                    * \n";
  const static std::string da= "*               (build "+ gtb + "-" + gtc + ")                        * \n";
  const static std::string e = "*                                                             * \n";
  const static std::string f = "* ----------------------------------------------------------- * \n";
  const static std::string g = "*                                                             * \n";
  const static std::string h = "*     Group of Computer Architecture (AGRA) - Uni Bremen      * \n";
  const static std::string i = "*   http://www.informatik.uni-bremen.de/agra/eng/index.php    * \n";
  const static std::string j = "*                                                             * \n";
  const static std::string k = "\\*************************************************************/ \n";
  const static std::string l = "								 \n";

  
  return a + b + c + d + da + e + f + g + h + i + j + k + l;
}

#endif

//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
