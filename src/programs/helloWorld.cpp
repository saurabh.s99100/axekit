// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : helloWorld.cpp
// @brief  : input is a verilog RTL output is a synthesised netlist, not
//           mapped to any technology lib and used internal pnemonics and
//           as such is useless.
//           Only to demonstrate the helloWorld, not good for anything else!
//------------------------------------------------------------------------------

#include "helloWorld.hpp"

using namespace axekit;

int main (int argc, char **argv) {
  using boost::program_options::value;
  program_options opts;
  
  std::string ifile = "kakakikikoo", ofile = "kakakikikoo";
  std::string top = "kakakikikoo";
  auto verbose = 1u;

  opts.add_options()
    ( "ifile,i",   value( &ifile ),  "Input Verilog RTL" )
    ( "ofile,o",   value( &ofile ),  "Output Verilog Netlist" )
    ( "top,t",     value( &top ),    "Name of the TOP Module" )
    ( "verbose,v", value_with_default( &verbose ),  "Turn off verbose" )
    ;
  opts.parse( argc, argv );
  if ( (top == "kakakikikoo") || (top == "kakakikikoo") ) {
    std::cerr << "Atleast give something..! (./helloWorld -i input.v -o output.v -t kiki)"
	      << std::endl;     // Should do these properly .. :)
    std::exit(-1);
  }
  
  //-----------------------------------------------------------
  std::cout << "A simple helloWorld synthesis!" << std::endl;
  //Yosys::yosys_setup(); // initialize yosys
  start_yosys();
  assert ( is_yosys_started() );
  if (verbose) std::cout << "[i] " << Yosys::yosys_version_str << std::endl;
  std::string work = "work";
  create_work_dir( work ); // Create the work directory.

  //-----------------------------------------------------------
  // 1. Read the verilog with Yosys and write out BLIF format.
  std::string out_blif = work + "/yosys.blif";
  std::string out_vlog = work + "/yosys.v";
  parse_verilog_to_ys (ifile, top);
  write_verilog_from_ys (out_vlog);
  write_blif_from_ys (out_blif);

  return 0;




  // 1. Read the verilog with Yosys and write out BLIF format.
  auto localdebug = false;
  //std::string out_blif = work + "/yosys.blif";
  Yosys::run_yosys_cmd("read_verilog  " + ifile, localdebug);
  auto design = Yosys::yosys_get_design();
  if (!Yosys::has_module ( top, design )) { // make sure module exists.
    std::cout << "[e] " << top << " not found in design!" << ifile << std::endl;
    assert(false);
  }
  Yosys::run_yosys_cmd("hierarchy -check -top " + top, localdebug);
  optimize_with_yosys(); // Do one round of opt with Yosys itself.
  Yosys::run_yosys_cmd("write_blif " + out_blif, localdebug);
  //-----------------------------------------------------------
  // 2. Fire up ABC to optimize further (resyn2)
  std::string opt_blif = work + "/abc.blif";
  abc::Abc_Start();
  abc::Abc_Frame_t *frame = abc::Abc_FrameGetGlobalFrame();
  if (!frame) {
    abc::Abc_Stop();
    std::cout << "[e] not able to invoke the ABC frame" << std::endl;
    assert (false ); 
  }
  run_abc_cmd (frame, "read_blif " + out_blif, localdebug);
  optimize_with_abc (frame);
  run_abc_cmd (frame, "write_blif " + opt_blif, localdebug);
  abc::Abc_Stop();
  //-----------------------------------------------------------
  // 3. Once again Yosys to write out the result.
  Yosys::run_yosys_cmd("design -reset ", localdebug);
  Yosys::run_yosys_cmd("read_blif  " + out_blif, localdebug);
  Yosys::run_yosys_cmd("write_verilog  " + ofile, localdebug);
  Yosys::yosys_shutdown();
  //-----------------------------------------------------------
  std::cout << "[i] Output netlist is at " << ofile << std::endl;
  return 0;
 
}


// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
