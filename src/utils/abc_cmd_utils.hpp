// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : abc_cmd_utils.hpp
//------------------------------------------------------------------------------
#pragma once

#ifndef ABC_CMD_UTILS_HPP
#define ABC_CMD_UTILS_HPP

#include <fstream>
#include <iostream>
#include <string>
#include <ext-libs/abc/abc_api.hpp>

namespace axekit {
  
void run_abc_cmd (abc::Abc_Frame_t *frame, std::string cmd, bool debug);
void abc_optimize (abc::Abc_Frame_t *frame);

}
#endif
//------------------------------------------------------------------------------

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
