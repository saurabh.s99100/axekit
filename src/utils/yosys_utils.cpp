// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : yosys_utils.cpp
// @brief  : Utilities using Yosys library.
//          
// In all these routines, subscript/superscript "ys" indicates related to
// yosys or the database inside yosys (correct name of it is RTLIL::*)
//
// Notes   : The yosys version used has some issues.
//   (a) Cannot be start-stop cycled. Once stoped cannot start again 
//   and run anything useful. Somehow crashes here.
//
//   (b) read_blif is a parser possibly coming from the original ABC but always 
//   assign "\\netlist" (the first "\\" is a must) name  to the top-module. 
//   This is modified and as such this is working.
//   But when trying to integrate the read-designs back, there are  other errors. 
//   Stopped developing read_blif with Yosys for the timebeing.
//
//   (c) Yosys will flatten out the bit-vectors in the LSB-MSB order
//    while declaring. But this is how its expected in all our flows including 
//    cirkit::mc_* routines and is the NORMAL behavior.
//    i.e. 
//    module (blah); output [2:0] blah; endmodule; 
//         ---YOSYS--->>> 
//             module ( blah[0], blah[1] ); output blah[0]; output blah[1]; endmodule; 
// 
//
// TODO: put proper yosys license headers
//------------------------------------------------------------------------------

#include "yosys_utils.hpp"

namespace axekit
{

// some variables to be used only in this file.
namespace { 
bool cirkit_yosys_status = false; // status of yosys initilaization
bool yosys_design_parse_status = false; // status of parse_verilog_to_ys
std::string top_module_in_ys = "NA";    // store the top module name.
bool design_netlist_status = false; // is design a netlist or contains "always" etc.
} 
//------------------------------------------------------------------------------
// Use always start_yosys() and stop_yosys() for initialization
// and not anything from Yosys:: namespace
void start_yosys () {
  Yosys::yosys_setup();
  cirkit_yosys_status = true;
}

void stop_yosys () {
  Yosys::yosys_shutdown();
  cirkit_yosys_status = false;
  yosys_design_parse_status = false;
  top_module_in_ys = "NA";
  design_netlist_status = false;
}

bool is_yosys_started () { 
  return cirkit_yosys_status; 
}
//------------------------------------------------------------------------------
// 1. resets the design whatever previously read in.
// 2. Does not reset the design after conversion. 
//    i.e. yosys database is still available further.
void verilog_to_blif_in_ys (const std::string &oblif, const std::string &ivlog, 
			    const std::string &top_module) {
  if ( !is_yosys_started() ) start_yosys();
  assert (is_yosys_started() && "Yosys not started!");
  auto design = Yosys::yosys_get_design();
  Yosys::Pass::call (design, Yosys::stringf("design -reset") );
  parse_verilog_to_ys (ivlog, top_module);
  write_blif_from_ys (oblif);
}

//------------------------------------------------------------------------------
void parse_verilog_to_ys (const std::string &filename, const std::string &top_module) {
  void ys_make_design_structural (); // Private function declaration
  assert (is_yosys_started() && "Yosys not started!");
  auto design = Yosys::yosys_get_design();
  Yosys::Frontend::frontend_call (design, nullptr, filename, "verilog");
  Yosys::Pass::call (design, Yosys::stringf("hierarchy -check -top %s", top_module.c_str()));
  yosys_design_parse_status = true;
  top_module_in_ys = top_module;
  ys_make_design_structural();

}
//------------------------------------------------------------------------------
// This routine does not need the RTL to be synthesised to a netlist.
void write_verilog_from_ys (std::string filename) {
  assert (is_yosys_started() && "Yosys not running!");
  assert (yosys_design_parse_status && "Verilog NOT parsed and converted to ys");
  auto design = Yosys::yosys_get_design();
  Yosys::Backend::backend_call (design, nullptr, filename, "verilog");
}

// RTL must be synthesised to a netlist, since BLIF only supports structural.
void write_blif_from_ys (std::string filename) {
  assert (is_yosys_started() && "Yosys not running!");
  assert (yosys_design_parse_status && "Verilog NOT parsed and converted to ys");
  assert (design_netlist_status && "Design NOT a netlist!");
  auto design = Yosys::yosys_get_design();
  Yosys::Backend::backend_call (design, nullptr, filename, "blif");
}
//------------------------------------------------------------------------------
unsigned get_port_width_in_ys ( const std::string &port_name ) {
  assert ( is_yosys_started() );
  auto design = Yosys::yosys_get_design();
  auto module = design->top_module();  // get the top module.
  for (auto &w : module->wires_) { // _wires is a dict in Module
    auto curr_port = ((w.second)->name).str();
    if ( port_name == Yosys::proper_name (curr_port) ) {
      auto width = (w.second)->width; 
      assert (width > 0 && width < 512); // for ridiculous problems and ports.
      return width;
    }
  }
  assert ( false && "[e] Error: Specified port not found in design!\n" );
  return 0u;
}

//------------------------------------------------------------------------------
std::string get_top_module_in_ys () { return top_module_in_ys; }
//------------------------------------------------------------------------------
// Does synthesis to make the design structural for CirKit purposes.
// TODO: check if the transformations are sufficient or not.
// TODO: consider making this public
void ys_make_design_structural () { // Private function
  assert (is_yosys_started() && "Yosys not started!");
  assert (yosys_design_parse_status && "Verilog NOT parsed and converted to ys");
  auto design = Yosys::yosys_get_design();
  
  // coarse synthesis
  Yosys::Pass::call(design, "proc");
  Yosys::Pass::call(design, "opt");
  Yosys::Pass::call(design, "wreduce");
  Yosys::Pass::call(design, "alumacc");
  Yosys::Pass::call(design, "share");
  Yosys::Pass::call(design, "opt");
  Yosys::Pass::call(design, "fsm");
  Yosys::Pass::call(design, "opt -fast");
  Yosys::Pass::call(design, "memory -nomap");
  Yosys::Pass::call(design, "opt_clean");

  // flatten design 
  Yosys::Pass::call(design, "flatten");
  Yosys::Pass::call(design, "opt_clean");

  // fine synthesis
  Yosys::Pass::call(design, "opt -fast -full");
  Yosys::Pass::call(design, "memory_map");
  Yosys::Pass::call(design, "opt -full");
  Yosys::Pass::call(design, "techmap");
  Yosys::Pass::call(design, "opt -fast");
  Yosys::Pass::call(design, "opt_clean");

  design_netlist_status = true;
}


//------------------------------------------------------------------------------
// Reads 1 verilog, 2 blifs, merges everything and finally write out the netlist 
// in BLIF format.
// Note: This is a standalone yosys invokation.
// Reading blif is NOT a yosys frontend, rather an ABC extension.
// ported from extern Yosys::RTLIL::Design * Yosys::abc_parse_blif (FILE *f, std::string dff_name);
// 
// Note: Should not use this function without testing.
// Tool crashes when tried to integrate back the result of blif-parsing.
// Need to debug further.
// 
// May be after removing start-stop recycling and design reset, this is still usable. 
// Not sure.

void convert_1v_and_2b_to_blif ( std::string ivlog, 
				 std::string iblif1, 
				 std::string iblif2, 
				 std::string oblif, std::string top_module )
{
  assert (false); // Immature, test properly before using.

  assert (is_yosys_started() && "Yosys not started!");

  // BLIF is a bit complicated. There is NO direct frontend call in Yosys for BLIF.
  // Hence parse with abc_parse_blif2 which retures a RTIL::Design, write out a 
  // verilog/ilang from it using the standard backend. And read back this verilog/ilang 
  // using standard frontend. 
  // Integrating an independent RTIL::Design to an existing design with correct 
  // hierarachies, net connections etc is NOT attempted.

  auto design = Yosys::yosys_get_design();
  Yosys::Pass::call (design, Yosys::stringf("design -reset") ); // first reset everything
  auto bf1_rtlil = abc_parse_blif2 (iblif1);
  Yosys::Backend::backend_call (bf1_rtlil, nullptr, "bf1.v", "verilog");
  auto bf2_rtlil = abc_parse_blif2 (iblif2);
  Yosys::Backend::backend_call (bf2_rtlil, nullptr, "bf2.v", "verilog");

  // Now read verilog and both the ilangs
  design = Yosys::yosys_get_design();

  // Another approach :: cat evrything together and read as 1 file.
  // cat_three_files ("tmp.v", "bf1.v", "bf2.v", ivlog);

  // Here there is an error in reading back the contents, tool ends.
  // No idea what it is. Probably solved with design -reset.
  Yosys::Frontend::frontend_call (design, nullptr, "bf1.v", "verilog");
  Yosys::Frontend::frontend_call (design, nullptr, "bf2.v", "verilog");
  Yosys::Frontend::frontend_call (design, nullptr, ivlog,    "verilog");

  Yosys::Pass::call (design, Yosys::stringf("hierarchy -check -top %s", top_module.c_str()));
  ys_make_design_structural();
  Yosys::Backend::backend_call (design, nullptr, oblif, "blif");

}

//------------------------------------------------------------------------------

// This is a helper function for abc_parse_blif2
// copied from original Yosys routine 
//     Yosys::RTLIL::Design * Yosys::abc_parse_blif (FILE *f, std::string dff_name);

static bool read_nxt_line(char *&buffer, size_t &buffer_size, int &line_count, FILE *f)
{
	int buffer_len = 0;
	buffer[0] = 0;

	while (1)
	{
		buffer_len += strlen(buffer + buffer_len);
		while (buffer_len > 0 && (buffer[buffer_len-1] == ' ' || buffer[buffer_len-1] == '\t' ||
				buffer[buffer_len-1] == '\r' || buffer[buffer_len-1] == '\n'))
			buffer[--buffer_len] = 0;

		if (buffer_size-buffer_len < 4096) {
			buffer_size *= 2;
			buffer = (char*)realloc(buffer, buffer_size);
		}

		if (buffer_len == 0 || buffer[buffer_len-1] == '\\') {
			if (buffer_len > 0 && buffer[buffer_len-1] == '\\')
				buffer[--buffer_len] = 0;
			line_count++;
			if (fgets(buffer+buffer_len, buffer_size-buffer_len, f) == NULL)
				return false;
		} else
			return true;
	}
}



// Always call with flattened and single module file.
// returns always a RTLIL Design with the module name of last .model statement in BLIF
// ported from original Yosys routine and modified.
//     Yosys::RTLIL::Design * Yosys::abc_parse_blif (FILE *f, std::string dff_name);
//
//
// There is NO direct frontend call in Yosys for BLIF.
// Rather use the routine below. Ported from an yosys routine itself (forgot the original file.)
//
// Note: returns an independednt RTLIL::Design. Need to integrate to 
// an existing design with correct  hierarachies, net connections etc.

Yosys::RTLIL::Design *abc_parse_blif2 (std::string filename) {
  auto f = std::fopen (filename.c_str(), "r");
  std::string dff_name = "\\_dff_";
  Yosys::RTLIL::Design *design = new Yosys::RTLIL::Design;
  Yosys::RTLIL::Module *module = new Yosys::RTLIL::Module;
  
  Yosys::RTLIL::Const *lutptr = NULL;
  Yosys::RTLIL::State lut_default_state = Yosys::RTLIL::State::Sx;
  

  size_t buffer_size = 4096;
  char *buffer = (char*)malloc(buffer_size);
  int line_count = 0;
  std::string blif_name = "\\netlist";

  // first extract the model name. 
  while (1) {
    if (!read_nxt_line(buffer, buffer_size, line_count, f))  goto error;
    if (buffer[0] == '#') continue;
    if (buffer[0] == '.') {
      char *cmd = strtok(buffer, " \t\r\n");
      if (!strcmp(cmd, ".model")) {
	char *p;
	while ((p = strtok(NULL, " \t\r\n")) != NULL) {
	  blif_name = std::string (p);
	}
	break;  
      }
    }
  }
  free(buffer);
  std::fclose (f);

  f = std::fopen (filename.c_str(), "r");
  std::cout << "in blif1\n";
  module->name = "\\" + blif_name;
  std::cout << "in blif2\n";
  design->add(module);

	buffer_size = 4096;
	buffer = (char*)malloc(buffer_size);
	line_count = 0;

	while (1)
	{
		if (!read_nxt_line(buffer, buffer_size, line_count, f))
			goto error;

	continue_without_read:
		if (buffer[0] == '#')
			continue;

		if (buffer[0] == '.')
		{
			if (lutptr) {
				for (auto &bit : lutptr->bits)
					if (bit == Yosys::RTLIL::State::Sx)
						bit = lut_default_state;
				lutptr = NULL;
				lut_default_state = Yosys::RTLIL::State::Sx;
			}

			char *cmd = strtok(buffer, " \t\r\n");

			if (!strcmp(cmd, ".model")) {
			  continue;  
			}
			

			if (!strcmp(cmd, ".end")) {
				module->fixup_ports();
				free(buffer);
				std::fclose (f);
				return design;
			}

			if (!strcmp(cmd, ".inputs") || !strcmp(cmd, ".outputs")) {
				char *p;
				while ((p = strtok(NULL, " \t\r\n")) != NULL) {
					Yosys::RTLIL::Wire *wire = module->addWire(Yosys::stringf("\\%s", p));
					if (!strcmp(cmd, ".inputs"))
						wire->port_input = true;
					else
						wire->port_output = true;
				}
				continue;
			}

			if (!strcmp(cmd, ".latch"))
			{
				char *d = strtok(NULL, " \t\r\n");
				char *q = strtok(NULL, " \t\r\n");

				if (module->wires_.count(Yosys::RTLIL::escape_id(d)) == 0)
					module->addWire(Yosys::RTLIL::escape_id(d));

				if (module->wires_.count(Yosys::RTLIL::escape_id(q)) == 0)
					module->addWire(Yosys::RTLIL::escape_id(q));

				Yosys::RTLIL::Cell *cell = module->addCell(NEW_ID, dff_name);
				cell->setPort("\\D", module->wires_.at(Yosys::RTLIL::escape_id(d)));
				cell->setPort("\\Q", module->wires_.at(Yosys::RTLIL::escape_id(q)));
				continue;
			}

			if (!strcmp(cmd, ".gate"))
			{
				char *p = strtok(NULL, " \t\r\n");
				if (p == NULL)
					goto error;

				Yosys::RTLIL::IdString celltype = Yosys::RTLIL::escape_id(p);
				Yosys::RTLIL::Cell *cell = module->addCell(NEW_ID, celltype);

				while ((p = strtok(NULL, " \t\r\n")) != NULL) {
					char *q = strchr(p, '=');
					if (q == NULL || !q[0] || !q[1])
						goto error;
					*(q++) = 0;
					if (module->wires_.count(Yosys::RTLIL::escape_id(q)) == 0)
						module->addWire(Yosys::RTLIL::escape_id(q));
					cell->setPort(Yosys::RTLIL::escape_id(p), module->wires_.at(Yosys::RTLIL::escape_id(q)));
				}
				continue;
			}

			if (!strcmp(cmd, ".names"))
			{
				char *p;
				Yosys::RTLIL::SigSpec input_sig, output_sig;
				while ((p = strtok(NULL, " \t\r\n")) != NULL) {
					Yosys::RTLIL::Wire *wire;
					if (module->wires_.count(Yosys::stringf("\\%s", p)) > 0) {
						wire = module->wires_.at(Yosys::stringf("\\%s", p));
					} else {
						wire = module->addWire(Yosys::stringf("\\%s", p));
					}
					input_sig.append(wire);
				}
				output_sig = input_sig.extract(input_sig.size()-1, 1);
				input_sig = input_sig.extract(0, input_sig.size()-1);

				if (input_sig.size() == 0) {
					Yosys::RTLIL::State state = Yosys::RTLIL::State::Sa;
					while (1) {
						if (!read_nxt_line(buffer, buffer_size, line_count, f))
							goto error;
						for (int i = 0; buffer[i]; i++) {
							if (buffer[i] == ' ' || buffer[i] == '\t')
								continue;
							if (i == 0 && buffer[i] == '.')
								goto finished_parsing_constval;
							if (buffer[i] == '0') {
								if (state == Yosys::RTLIL::State::S1)
									goto error;
								state = Yosys::RTLIL::State::S0;
								continue;
							}
							if (buffer[i] == '1') {
								if (state == Yosys::RTLIL::State::S0)
									goto error;
								state = Yosys::RTLIL::State::S1;
								continue;
							}
							goto error;
						}
					}
				finished_parsing_constval:
					if (state == Yosys::RTLIL::State::Sa)
						state = Yosys::RTLIL::State::S1;
					module->connect(Yosys::RTLIL::SigSig(output_sig, state));
					goto continue_without_read;
				}

				Yosys::RTLIL::Cell *cell = module->addCell(NEW_ID, "$lut");
				cell->parameters["\\WIDTH"] = Yosys::RTLIL::Const(input_sig.size());
				cell->parameters["\\LUT"] = Yosys::RTLIL::Const(Yosys::RTLIL::State::Sx, 1 << input_sig.size());
				cell->setPort("\\A", input_sig);
				cell->setPort("\\Y", output_sig);
				lutptr = &cell->parameters.at("\\LUT");
				lut_default_state = Yosys::RTLIL::State::Sx;
				continue;
			}

			goto error;
		}

		if (lutptr == NULL)
			goto error;

		char *input = strtok(buffer, " \t\r\n");
		char *output = strtok(NULL, " \t\r\n");

		if (input == NULL || output == NULL || (strcmp(output, "0") && strcmp(output, "1")))
			goto error;

		int input_len = strlen(input);
		if (input_len > 8)
			goto error;

		for (int i = 0; i < (1 << input_len); i++) {
			for (int j = 0; j < input_len; j++) {
				char c1 = input[j];
				if (c1 != '-') {
					char c2 = (i & (1 << j)) != 0 ? '1' : '0';
					if (c1 != c2)
						goto try_next_value;
				}
			}
			lutptr->bits.at(i) = !strcmp(output, "0") ? Yosys::RTLIL::State::S0 : Yosys::RTLIL::State::S1;
		try_next_value:;
		}

		lut_default_state = !strcmp(output, "0") ? Yosys::RTLIL::State::S1 : Yosys::RTLIL::State::S0;
	}

error:
	std::fclose (f);
	std::cout << "Syntax error in line "<< line_count << " in file " << filename << "!\n";
	assert (false && "BLIF READ ERROR");
	return nullptr;
} 

 

} // namespace axekit

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
