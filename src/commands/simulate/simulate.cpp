// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : simulate.cpp
// @brief  : 
//           
//           
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// The order of the result is the most confusing.
/*
//------------------------------------------------------------------------------
module dummy (input  [1:0] a, input b, output[2:0] out, output y);
  assign out[0] = ~a[0];
  assign out[2] = 0;
  assign out[1] = b;
  assign y = 1;
endmodule
//------------------------------------------------------------------------------
aXc> read_verilog dummy.v -t dummy; aig_to_ntk; simulate -s 2 --all
input   :   output
000   :   1001
001   :   1000
010   :   1001
011   :   1000
100   :   1011
101   :   1010
110   :   1011
111   :   1010

//------------------------------------------------------------------------------
i.e. taken as 
b a[1] a[0]  :   y out[2] out[1] out[0]

OR (MSB:LSB) : MSB -> is the order.

*/
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

#include "simulate.hpp"

#include <fstream>
#include <vector>
#include <algorithm>
#include <boost/lexical_cast.hpp>
#include <boost/dynamic_bitset.hpp>

namespace alice
{

namespace {
void write_result ( const std::string &filename, const std::vector<std::string> &results,
		    const unsigned nPi ){
  std::ofstream ofs (filename);
  ofs << "input : output\n";
  for (auto i=0u; i < results.size(); i++) {
    auto bs = boost::dynamic_bitset<> (nPi, i);
    auto output = boost::dynamic_bitset<> ( results[i] );
    ofs << bs << "  :  " << results[i] << " (" << output.to_ulong() << ")\n";
  }
}

void write_result ( const std::string &filename, const std::string &result,
		    const std::vector<int> &pi_vals ) {
  std::ofstream ofs (filename);
  ofs << "input : output\n";
  for (auto i=(pi_vals.size() - 1); i >= 0; i--)
    ofs << pi_vals[i];

  auto output = boost::dynamic_bitset<> ( result );
  ofs << "  :  " << result <<  " (" << output.to_ulong() << ")\n";
}


}

simulate_command::rules_t simulate_command::validity_rules() const {
  return {
    { [this]() {
	return store == 0u || store == 2u;
      }, "Only Aig/Ntk supported currently. Use store = 0 or 2" },

      
    { [this]() {
	if (2u == store) { // Ntk store
	  const auto& ntks = env->store<abc::Abc_Ntk_t*>();
	  return id < ntks.size() ;
	}
	else if (0u == store) { // AIG store
	  auto& gias = env->store<abc::Gia_Man_t*> ();
	  return id < gias.size();	  
	}
	else { // BDD store
	  return false;
	}
      }, "Store id out of range" },

    { [this]() {
	return ( is_set ("all") || is_set ("pattern") || is_set ("number") );
      }, "Should give either --all or --pattern or --number" },


    { [this]() {
	if ( is_set ("all") && is_set ("pattern") ) return false;
	else if ( is_set ("all") && is_set ("number") ) return false;
	else if ( is_set ("pattern") && is_set ("number") ) return false;
	else return true;
      }, "Give only one option: either --all or --pattern or --number" }

  };
}

bool simulate_command::execute() {
  abc::Abc_Ntk_t *ntk;
  if (2 == store) { // Ntk
    auto& ntks = env->store<abc::Abc_Ntk_t*> ();
    ntk = ntks[id];
  } else if ( 0 == store ) { // Gia
    auto& gias = env->store<abc::Gia_Man_t*> ();
    auto gia = gias[id];
    ntk = axekit::convert_gia_to_ntk ( gia );
    assert ( abc::Abc_NtkLatchNum (ntk) == 0 ); // seq not supported for now.
    
  } else {
    std::cout << "[e] Unsupported store!\n";
    return false;
  }

  const auto nPi = abc::Abc_NtkPiNum (ntk);
  const auto nPo = abc::Abc_NtkPoNum (ntk); 


  if ( is_set ("all") ) {
    if (nPi > 16) {
      std::cout << "[e] with --all, input size more than 16 not supported!\n";
      return false;
    }
    std::vector <std::string> all_results;
    const unsigned long one = 1u;
    const unsigned long nPatterns = (one << nPi) - 1;
    auto curr_num = 0;
    std::vector<int> vals(nPi, 0);

    while ( curr_num <=  nPatterns ) {
      auto res = abc::Abc_NtkVerifySimulatePattern ( ntk, &vals[0] );
      std::string result;
      /*for (auto i = nPo-1; i >= 0; i--) {
	result = result + std::to_string ( res[i] );
	}*/

      for (auto i=0u; i<nPo; i++) {
	result = result + std::to_string ( res[i] );
      }

      // dont touch the reverse
      std::reverse ( result.begin(), result.end() );
      all_results.emplace_back ( result );
      curr_num++;
      auto num = curr_num;
      for (auto i=0u; i<nPi; i++) {
	vals[i] = num & 1;
	num = num >> 1;
      }
    }

    if ( is_set("filename") ) { // print to file 
      write_result (file, all_results, nPi);
      std::cout << "[i] Simulation results wrote to " << file << "\n";
      return true;
    }
    // or print to console
    std::cout << "input   :   output\n";
    for (auto i=0u; i < all_results.size(); i++) {
      auto input  = boost::dynamic_bitset<> ( nPi, i );
      auto output = boost::dynamic_bitset<> ( all_results[i] );
      std::cout << input << "   :   " << all_results[i] << "\n";
    }
    
    return true;
  }


  // TODO: get rid of this bitset. no need of it.
  // Do something similar to "all" above.
  boost::dynamic_bitset<> bs;
  
  if ( is_set("pattern") ) {
    try {
      bs = boost::lexical_cast< boost::dynamic_bitset<> > (pattern);
    }
    catch (boost::bad_lexical_cast &) {
      std::cout << "[e] Is " << pattern << " a valid signal pattern (1 and 0)?\n";
      return false;
    }
  }
  else if ( is_set("number") ) {
    try {
      bs = boost::lexical_cast< boost::dynamic_bitset<> > (num);
    }
    catch (boost::bad_lexical_cast &) {
      std::cout << "[e] Is " << num << " a valid number?\n";
      return false;
    }
  }
  
  if (bs.size() > nPi) {
    std::cout << "[w] Given input has a bit-width [" <<bs.size() << "] more than number of PI ["
	      << nPi << "]. Excess MSB bits discarded\n";
  }
  if (bs.size() < nPi) {
    std::cout << "[w] Given input has a bit-width [" <<bs.size() << "] less than number of PI ["
	      << nPi << "]. Extra 0 vals prefixed\n";
  }

  std::vector<int> pi_vals(nPi, 0);
  //int pi_vals[nPi];
  for (auto i=0u; i<nPi; i++) {
    if ( i > (bs.size() - 1) ) break;
    pi_vals[i] = bs[i];
  }
    
  auto res = abc::Abc_NtkVerifySimulatePattern ( ntk, &pi_vals[0] );
  std::string result;
  for (auto i = nPo-1; i >= 0; i--)
    result = result + std::to_string ( res[i] );

  // print to both file and console
  std::cout << "Result = " << result << "\n"; 
  if ( is_set("filename") )  write_result (file, result, pi_vals);
  
  return true;
}


simulate_command::log_opt_t simulate_command::log() const  {
  return log_opt_t (
    {
      {"file", file},
      {"store_used", (store == 0u) ? std::string("AIG") : std::string("NTK") },
      {"store_id", id }

    }
    );
}


} // namespace alice
//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
