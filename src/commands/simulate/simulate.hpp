// Copyright (C) AGRA - University of Bremen
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : simulate.hpp
// @brief  : 
//
//------------------------------------------------------------------------------
#pragma once

#ifndef SIMULATE_HPP
#define SIMULATE_HPP

#include <commands/axekit_stores.hpp>
#include <utils/common_utils.hpp>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>
#include <ext-libs/abc/abc_api.hpp>

namespace alice {

class simulate_command : public command {
public:
  simulate_command (const environment::ptr &env) : command (env, "Does something")
  {
    add_positional_option ("filename");
    opts.add_options()
      ( "id", po::value( &id )->default_value( id ), "store id of the circuit" )
      ( "store,s", po::value( &store )->default_value( store ), "Which store to use? (Aig=0 / Ntk=2)" )
      ( "pattern,p",   po::value (&pattern),
	"Input signal values to simulate. Left out MSB bits are filled with 0" )
      ( "number,n",   po::value (&num),
	"Input unsigned value to simulate. Expanded to input bit pattern in the PI order. Bits truncated if more than number of PIs" )
      ( "all,a",     "Do exhaustive input simulation. (Currently restricted to 8-bit inputs only) " )
      ( "filename,f",   po::value (&file),         "The output pattern dump" )
      ;
  }

protected:
  // these 3 funtions, validity_rules(), execute() and log() defines the behavior of the command.
  // keep this as it is, in every command header. log() and validity_rules() are optional.
  
  /* rules to check before execution:
   *
   * rules_t is a vector of pairs, each pair is a predicate and a string:
   * predicates are checked in order, if they are false, the command is not executed and the string is
   * outputted as error.
   */
  rules_t validity_rules() const;

  // the main execution part of the command.
  bool execute(); 

  // Log for tool execution in JSON format
  log_opt_t log() const;

private:
  std::string file = "NA";
  std::string pattern;
  unsigned num;
  unsigned id = 0;
  unsigned store = 0;
  
};

}

//------------------------------------------------------------------------------

#endif
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
