// Copyright (C) AGRA - University of Bremen
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : template.hpp
// @brief  : 
//
//------------------------------------------------------------------------------
#pragma once

#ifndef TEMPLATE_HPP
#define TEMPLATE_HPP

#include <commands/axekit_stores.hpp>

namespace alice {

class template_command : public command {
public:
  template_command (const environment::ptr &env) : command (env, "Does something")
  {
    add_positional_option ("filename");
    opts.add_options()
      ( "filename,f",   po::value (&file),         "The input Verilog file" )
      ( "new,n" ,                                  "Create a new store entry" )
      ;
  }

protected:
  // these 3 funtions, validity_rules(), execute() and log() defines the behavior of the command.
  // keep this as it is, in every command header. log() and validity_rules() are optional.
  
  /* rules to check before execution:
   *
   * rules_t is a vector of pairs, each pair is a predicate and a string:
   * predicates are checked in order, if they are false, the command is not executed and the string is
   * outputted as error.
   */
  rules_t validity_rules() const;

  // the main execution part of the command.
  bool execute(); 

  // Log for tool execution in JSON format
  log_opt_t log() const;

private:
  std::string file;
};

}

//------------------------------------------------------------------------------

#endif
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
