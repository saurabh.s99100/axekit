// Copyright (C) AGRA - University of Bremen
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : aiger_io.cpp
// @brief  : reads/write in aig file in Gia format
//
//------------------------------------------------------------------------------

#include "aiger_io.hpp"
#include <ext-libs/abc/abc_api.hpp>
#include <ext-libs/yosys/yosys_api.hpp>
#include <fstream>
#include <iostream>

namespace alice
{

namespace {
// Private functions.
// predicate for validity-rule 1.
bool check_extension (const std::string &file) {
  auto ext = axekit::get_extension_of_file (file);
  if (ext == ".aig") return true;
  if (ext == ".AIG") return true;
  return false;
}

}

read_aiger_command::rules_t read_aiger_command::validity_rules() const {
  return {
    // rules are pairs, first is a predicate and, next is a string when predicate is false.
    // RULE FORMAT::    { [this]() {} , ""  }

    { [this]() {
	boost::filesystem::path p(file);
	return boost::filesystem::exists (p);
      }
      , "Where is the input file?"  },

   // rule-1. file should have aiger extension.
    { [this]() { return check_extension (file); }
      , "Not an AIGER? Extension should be .aig or .AIG!"  },
    { [this]() { return (store == 2 || store == 0) ; }
      , "Store must be 0 (Gia) or 2 (Old Aig Network)"  }
  };
}

bool read_aiger_command::execute() {
  auto begin_time = std::clock ();

  char name[] = "temp";
  if (0 == store) {
    // do the below in 2 steps. Else errors will not be easy to understand.
    auto gia = abc::Gia_AigerRead( (char *)file.c_str(), 1, 1, 1 );
    gia->pName = abc::Abc_UtilStrsav ( name ); 
    if (gia == nullptr) return false;
    if ( gia->nRegs > 0 )  std::cout << "[i] Read in circuit is sequential.\n";
    auto& gias = env->store <abc::Gia_Man_t *>();
    if ( is_set("new") || gias.empty() ) {
      gias.extend();
    }
    gias.current() = gia;
    t_read = get_elapsed_time (begin_time);
    return true;
  }
  
  if (2 == store) {
    auto ntk = abc::Io_ReadAiger ( (char *)file.c_str(), 1 );
    ntk = abc::Abc_NtkToLogic (ntk); // Convert to Logic form.
    ntk = abc::Abc_NtkStrash (ntk, 1, 1, 0); // Strash
    abc::Abc_AigCleanup((abc::Abc_Aig_t *)ntk->pManFunc); // Cleanup
    ntk = abc::Abc_NtkStrash (ntk, 1, 1, 0); // Strash again
    if (ntk == nullptr) return false;
    ntk->pName = abc::Abc_UtilStrsav ( name ); 
    if ( abc::Abc_NtkLatchNum (ntk) != 0 )
      std::cout << "[i] Read in circuit is sequential.\n";
 
    auto& aigs = env->store <abc::Abc_Ntk_t *>();
    if ( is_set("new") || aigs.empty() ) {
      aigs.extend();
    }
    aigs.current() = ntk;
    t_read = get_elapsed_time (begin_time);
    return true;
  }

  return false; // shud not reach this stage.
  
}

read_aiger_command::log_opt_t read_aiger_command::log() const  {
  return log_opt_t (
    {
      {"file", file},
      {"store_used", (store == 0u) ? std::string("GIA") : std::string("NTK") },
      {"time_taken", boost::lexical_cast<std::string> (t_read) }
	      
    }
    );
}

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------

write_aiger_command::rules_t write_aiger_command::validity_rules() const {
  return {
    { [this]() { return (store == 2 || store == 0) ; }
      , "Store must be 0 (Gia) or 2 (Old Aig Network)"  }
  };
}

bool write_aiger_command::execute() {
  auto begin_time = std::clock ();
  
  if (0 == store) {
    auto& gias = env->store <abc::Gia_Man_t *>();
    if ( gias.empty() ) {
      std::cout << "[e] Empty Gia store\n";
      return false;
    }
    if ( id >= gias.size() ) {
      std::cout << "[e] Id out of range\n";
      return false;
    }
    auto gia = gias[id];
    if (gia == nullptr) return false;
    
    abc::Gia_AigerWrite ( gia, (char*)file.c_str(), 1, 0 );
    t_write = get_elapsed_time (begin_time);
    return true;
  }
  
  if (2 == store) {
    auto& ntks = env->store <abc::Abc_Ntk_t *>();
    if ( ntks.empty() ) {
      std::cout << "[e] Empty Network store\n";
      return false;
    }
    if ( id >= ntks.size() ) {
      std::cout << "[e] Id out of range\n";
      return false;
    }
    auto ntk = ntks[id];
    if (ntk == nullptr) return false;

    ntk = abc::Abc_NtkStrash (ntk, 1, 1, 0); // Strash
    abc::Abc_AigCleanup((abc::Abc_Aig_t *)ntk->pManFunc); // Cleanup
    ntk = abc::Abc_NtkStrash (ntk, 1, 1, 0); // Strash again
    if (ntk == nullptr) return false;
    abc::Io_WriteAiger (ntk, (char *)file.c_str(), 1, 0, 0);
    t_write = get_elapsed_time (begin_time);
    return true;
  }

  return false; // shud not reach this stage.
  
}

write_aiger_command::log_opt_t write_aiger_command::log() const  {
  return log_opt_t (
    {
      {"file", file},
      {"store_used", (store == 0u) ? std::string("GIA") : std::string("NTK") },
      {"time_taken", boost::lexical_cast<std::string> (t_write) }
    }
    );
}


} // namespace alice



// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
