// Copyright (C) AGRA - University of Bremen
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : compile.hpp
// @brief  : 
//
//------------------------------------------------------------------------------
#pragma once

#ifndef COMPILE_HPP
#define COMPILE_HPP

#include <commands/axekit_stores.hpp>
#include <cirkit/cudd_cirkit.hpp>
#include <functions/bdd/bdd_error_metrics.hpp>
#include <functions/compile/aig_rewrite.hpp>
#include <functions/compile/aig_first_cut.hpp>
#include <functions/compile/aig_random_cut.hpp>
#include <utils/common_utils.hpp>
#include <ext-libs/abc/abc_api.hpp>


#include <fstream>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>
#include <boost/lexical_cast.hpp>

namespace alice {

class compile_command : public command {
public:
  compile_command (const environment::ptr &env) : command (env, "Approximation Synthesis")
  {
    add_positional_option ("filename");
    opts.add_options()
      ( "id",         po::value( &id )->default_value( id ), "store id of original circuit" )
      ( "store,s",    po::value( &store )->default_value( store ), "Which store to use? (Aig=0 / BDD=1 / Ntk=2)" )
      ( "new,n" ,                         "Create a new store entry" )
      ( "rewrite" ,                       "Use AIG rewrite algorithm (ICCAD-2016). Only rewriting is supported for now." )
      ( "simple" ,                        "Do a simple AIG rewrite approximation. Ignores all error metrics, but reduces area/delay." )
      ( "random" ,                        "Do a random AIG rewrite approximation. Ignores all error metrics, may reduces area/delay." )
      ( "wc",         po::value( &wc )->default_value(wc), "Worst-Case error limit input (-ve or 0 to disable)" )
      ( "ac",         po::value( &ac )->default_value(ac), "Average-Case error limit input (-ve or 0 to disable)" )
      ( "er_frac",    po::value( &er_frac )->default_value(er_frac), "Error-Rate limit input in fraction (0.5 is 50% error rate, -ve or 0 to disable)" )
      ( "bf",         po::value( &bf )->default_value(bf), "Max-Bit-Flips error limit input (-ve or 0 to disable)" )
      ( "debug",      po::value (&debug)->default_value( debug ), "Set debug levels" )
      ( "report,r",   po::value (&rpt_file)->default_value( rpt_file ), "Report file to be written" )
      ;
  }

  
protected:
  // these 3 funtions, validity_rules(), execute() and log() defines the behavior of the command.
  // keep this as it is, in every command header. log() and validity_rules() are optional.
  
  /* rules to check before execution:
   *
   * rules_t is a vector of pairs, each pair is a predicate and a string:
   * predicates are checked in order, if they are false, the command is not executed and the string is
   * outputted as error.
   */
  rules_t validity_rules() const;

  // the main execution part of the command.
  bool execute(); 

  // Log for tool execution in JSON format
  log_opt_t log() const;

private:
  std::string rpt_file = "AxC_compile.rpt";
  unsigned id = 0;
  int store = 2;

  // input values.
  boost::multiprecision::cpp_dec_float_100 er_frac = -1;
  boost::multiprecision::int256_t wc   = -1;
  boost::multiprecision::cpp_dec_float_100 ac = -1;
  boost::multiprecision::int256_t bf = -1;  

  float arw_sort_paths_time = -1;
  float arw_applycut_time = -1;
  float arw_generatecuts_time = -1;


  bool wc_flag = false;
  bool ac_flag = false;
  bool er_flag = false;
  bool bf_flag = false;
  bool wc_bf_flag = false;
  
  void write_report();
  void set_flags();
  void clear_all();

  unsigned debug = 0u;

};

}

//------------------------------------------------------------------------------

#endif
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
