// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : gia_error_metrics.cpp
// @brief  : functions to report error metrics using gia.
//------------------------------------------------------------------------------

#include "gia_error_metrics.hpp"


#include <ext-libs/sharpsat/sharpsat.hpp>

namespace axekit
{

using mpfloat = boost::multiprecision::cpp_dec_float_100;
using mpuint = boost::multiprecision::uint256_t;

mpuint worst_case ( abc::Gia_Man_t *fs, abc::Gia_Man_t *fshat ) {
  auto diff = subtract_gia (fs, fshat);

#ifdef DEBUG_WITH_BDD_WC
  // Conclusion: get_max_value() procedure is correct with Gia and BDD.
  char tmpv[] = "gia_diff.v";
  auto kk = abc::Gia_ManToAig (diff, 0);
  abc::Aig_ManDumpVerilog (kk, tmpv);    
  
  Cudd mgr;
  cirkit::bdd_simulator simulator( mgr );
  auto aig = cirkit::gia_to_cirkit (diff);
  auto values = cirkit::simulate_aig( aig, simulator );
  std::vector<BDD> bfs;
  for ( const auto& o : cirkit::aig_info( aig ).outputs ) {
    bfs.push_back( values[o.first] );
  }
  std::cout << "Max Value with BDD = " << get_max_value ( {mgr, bfs} ) << "\n";
#endif

  auto result = get_max_value (diff);
  abc::Gia_ManStop ( diff );   // clean up 
  return result;
}


// Change EVALUATED_MAX_VALUES for more accuracy.
mpfloat average_case ( abc::Gia_Man_t *fs, abc::Gia_Man_t *fshat,
		       const mpuint &evaluated_max_values, const unsigned tool_type ) {
  assert ( abc::Gia_ManPoNum (fs) == abc::Gia_ManPoNum (fshat) );
  assert ( abc::Gia_ManPiNum (fs) == abc::Gia_ManPiNum (fshat) );
  const mpuint one = 1;
  auto diff = subtract_gia ( fs, fshat );
  auto sum = mpfloat ( sum_of_max_values ( diff, evaluated_max_values, tool_type ) );
  //std::cout << "Sum returned = " << sum << "\n";
  return sum / mpfloat ( one << abc::Gia_ManPiNum (fs) );
}


// This routine is not the one used in total_arithemetic_error command.
mpuint sum_of_errors ( abc::Gia_Man_t *diff ) {
  return sum_of_max_values (diff, 100); // tool_type is aXc
}

// Interface to sharpsat is an explictily written out CNF file.
mpuint error_rate_with_sharpsat_and_cnffile ( abc::Gia_Man_t *fs,
					      abc::Gia_Man_t *fshat ) {
  auto xg = xor_gia (fs, fshat);
  auto oio = or_individual_outputs ( xg );
  assert ( abc::Gia_ManPoNum(oio) == 1); // Must be single output miter.
  const mpuint one (1);

  if ( abc::Gia_ManPoIsConst0 (oio, 0) ) { // Direct UNSAT
    abc::Gia_ManStop (oio); // clean up
    abc::Gia_ManStop (xg); // clean up 
    return mpuint(0); 
  }
  if ( abc::Gia_ManPoIsConst1 (oio, 0) ) { // all input combinations.
    abc::Gia_ManStop (oio); // clean up
    abc::Gia_ManStop (xg); // clean up
    return ( one << abc::Gia_ManPiNum(fs) );  // Note: should not use oio here.
  }
  auto cnf = abc::Mf_ManGenerateCnf (oio, 8, 0, 0, 0);
  char cnffile[] = ".sharpsat_er_oio.cnf";
  const auto fReadable = 1;
  const auto num_outputs = 1;
  const auto num_inputs = abc::Gia_ManPiNum(oio);
  auto unused_inputs = write_cnf ( cnf, cnffile, fReadable, num_inputs, num_outputs );
  abc::Cnf_DataFree( cnf );

  mpuint result = one << unused_inputs ;
  
  auto solver = new SharpSATSolver();
  solver->solve ( std::string (cnffile) );
    
  if ( solver->status() )  result = result * mpuint ( solver->model_count().get_ui() );
  abc::Gia_ManStop (oio); // clean up
  abc::Gia_ManStop (xg);  // clean up
  return result;
}

// sharpsat datastructures are directly derived from the Cnf, without any I/O.
mpuint error_rate_with_sharpsat ( abc::Gia_Man_t *fs, abc::Gia_Man_t *fshat ) {
  auto xg = xor_gia (fs, fshat);
  auto oio = or_individual_outputs ( xg );
  assert ( abc::Gia_ManPoNum(oio) == 1); // Must be single output miter.
  const mpuint one (1);

  if ( abc::Gia_ManPoIsConst0 (oio, 0) ) { // Direct UNSAT
    abc::Gia_ManStop (oio); // clean up
    abc::Gia_ManStop (xg);  // clean up 
    return mpuint(0); 
  }
  if ( abc::Gia_ManPoIsConst1 (oio, 0) ) { // all input combinations.
    abc::Gia_ManStop (oio); // clean up
    abc::Gia_ManStop (xg);  // clean up
    return ( one << abc::Gia_ManPiNum(fs) );  // Note: should not use oio here.
  }
  

  auto solver = new SharpSATSolver();
  auto cnf = abc::Mf_ManGenerateCnf (oio, 8, 0, 0, 0);
  const auto fReadable = 1;
  const auto num_outputs = 1;
  const auto num_inputs = abc::Gia_ManPiNum(oio);
  auto unused_inputs = solver->writeCnfData ( cnf, fReadable, num_inputs, num_outputs );
  abc::Cnf_DataFree( cnf );

  mpuint result = one << unused_inputs ;
  
  solver->solve ();
    
  if ( solver->status() )  result = result * mpuint ( solver->model_count().get_ui() );
  abc::Gia_ManStop (oio); // clean up
  abc::Gia_ManStop (xg);  // clean up
  return result;
}

// Interface to cryptoMiniSAT is an explictily written out CNF file.
mpuint error_rate_with_cms_and_cnffile ( abc::Gia_Man_t *fs, abc::Gia_Man_t *fshat ) {
  auto xg = xor_gia (fs, fshat);
  auto oio = or_individual_outputs ( xg );
  assert ( abc::Gia_ManPoNum(oio) == 1); // Must be single output miter.
  const mpuint one (1);

  if ( abc::Gia_ManPoIsConst0 (oio, 0) ) { // Direct UNSAT
    abc::Gia_ManStop (oio); // clean up
    abc::Gia_ManStop (xg); // clean up 
    return mpuint(0); 
  }
  if ( abc::Gia_ManPoIsConst1 (oio, 0) ) { // all input combinations.
    abc::Gia_ManStop (oio); // clean up
    abc::Gia_ManStop (xg); // clean up
    return ( one << abc::Gia_ManPiNum(fs) );  // Note: should not use oio here.
  }
  auto cnf = abc::Mf_ManGenerateCnf (oio, 8, 0, 0, 0);
  char cnffile[] = ".cms_er_oio.cnf";
  const auto fReadable = 1;
  const auto num_outputs = 1;
  const auto num_inputs = abc::Gia_ManPiNum(oio);
  auto unused_inputs = write_cnf ( cnf, cnffile, fReadable, num_inputs, num_outputs );
  abc::Cnf_DataFree( cnf );

  mpuint result = one << unused_inputs ;
 
  const auto argc = 6;
  char *argv[] = {(char *)"mc_solver", (char *)"--pivotAC",
		  (char *)"60", (char *)"--tApproxMC", (char *)"1", cnffile };
  CUSP mc_solver (argc, argv);
  mc_solver.conf.verbStats = 0;
  mc_solver.parseCommandLine();
  auto mc_status = mc_solver.solve();
  mpuint nn ( mc_solver.get_cell_sol_count() );
  //std::cout << nn << " x 2^" << mc_solver.get_hash_count() << "\n";
  result = result * nn * ( one << mc_solver.get_hash_count() );

#ifdef DEBUG_WITH_BDD_CMS_ER
    Cudd mgr;
    cirkit::bdd_simulator simulator( mgr );
    auto aig = cirkit::gia_to_cirkit (oio);
    auto values = cirkit::simulate_aig( aig, simulator );
    std::vector<BDD> bfs;
    for ( const auto& o : cirkit::aig_info( aig ).outputs ) {
      bfs.push_back( values[o.first] );
    }
    auto h = bfs[0];
    std::stringstream s;
    s.precision(0);
    s << std::fixed << h.CountMinterm( mgr.ReadSize() );
    return boost::multiprecision::uint256_t( s.str() );
#endif
  
  abc::Gia_ManStop (oio); // clean up
  abc::Gia_ManStop (xg); // clean up
  return result;

}

// CMS datastructures are directly derived, not through I/O
// This is an involved work. deferred for now.
mpuint error_rate_with_cms ( abc::Gia_Man_t *fs, abc::Gia_Man_t *fshat ) {
  assert (false && "not ready\n");
  auto xg = xor_gia (fs, fshat);
  auto oio = or_individual_outputs ( xg );
  assert ( abc::Gia_ManPoNum(oio) == 1); // Must be single output miter.
  const mpuint one (1);

  if ( abc::Gia_ManPoIsConst0 (oio, 0) ) { // Direct UNSAT
    abc::Gia_ManStop (oio); // clean up
    abc::Gia_ManStop (xg); // clean up 
    return mpuint(0); 
  }
  if ( abc::Gia_ManPoIsConst1 (oio, 0) ) { // all input combinations.
    abc::Gia_ManStop (oio); // clean up
    abc::Gia_ManStop (xg); // clean up
    return ( one << abc::Gia_ManPiNum(fs) );  // Note: should not use oio here.
  }
  
  return one;
}


mpuint error_rate ( abc::Gia_Man_t *fs, abc::Gia_Man_t *fshat ) {
  assert (false && "Use error_rate_with_sharpsat() or error_rate_with_cms()");
  
  auto xg = xor_gia (fs, fshat); // this does the same as abc::Gia_ManMiter()
  //auto xg = abc::Gia_ManMiter ( fs, fshat, 0, 0, 0, 0, 0 );
  auto oio = or_individual_outputs ( xg );
  assert ( abc::Gia_ManPoNum(oio) == 1); // Must be single output miter.
  const mpuint one (1);
  if ( abc::Gia_ManPoIsConst0 (oio, 0) ) { // Direct UNSAT
    abc::Gia_ManStop (oio); // clean up
    abc::Gia_ManStop (xg); // clean up 
    return mpuint(0); 
  }
  if ( abc::Gia_ManPoIsConst1 (oio, 0) ) { // all input combinations.
    abc::Gia_ManStop (oio); // clean up
    abc::Gia_ManStop (xg); // clean up
    return ( one << abc::Gia_ManPiNum(oio) ); 
  }

  auto cnf = abc::Mf_ManGenerateCnf (oio, 8, 0, 0, 0);
  char cnffile[] = ".er_oio.cnf";
  const auto fReadable = 1;
  const auto num_outputs = 1;
  const auto num_inputs = abc::Gia_ManPiNum(oio);
  auto unused_inputs = write_cnf ( cnf, cnffile, fReadable, num_inputs, num_outputs );
  abc::Cnf_DataFree( cnf );

  
  auto count = 1lu << unused_inputs;
  std::cout << " Multiply with " << count << "\n";
  return 0;

#ifdef ER_DEBUG_WITH_BDD
  // The final xor of each outputs    
  Cudd mgr;
  cirkit::bdd_simulator simulator( mgr );
  auto aig = cirkit::gia_to_cirkit (xg);
  auto values = cirkit::simulate_aig( aig, simulator );
  std::vector<BDD> bfs;
  for ( const auto& o : cirkit::aig_info( aig ).outputs ) {
    bfs.push_back( values[o.first] );
  }
  auto h = mgr.bddZero();
  for ( auto i = 0u; i < bfs.size(); ++i )
  {
    h |= bfs[i];
  }
  
  std::stringstream s;
  s.precision(0);
  s << std::fixed << h.CountMinterm( mgr.ReadSize() );
  std::cout << "Solution with BDD = " << mpuint ( s.str() ) << "\n";
#endif
  
  abc::Gia_ManStop (xg); // clean up
  abc::Gia_ManStop (oio); // clean up
  return 0;
}

mpuint bit_flip_error (abc::Gia_Man_t *fs, abc::Gia_Man_t *fshat) {
  auto xg = xor_gia (fs, fshat);
  auto fsum = add_individual_outputs ( xg );
  auto result = get_max_value (fsum);
  abc::Gia_ManStop (fsum); // clean up
  abc::Gia_ManStop (xg);   // clean up
  return result;
}

} // namespace axekit

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
