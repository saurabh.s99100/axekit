// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : gia_bmc.hpp
// @brief  : Bounded Model Checking
//          
//------------------------------------------------------------------------------
#pragma once
#ifndef GIA_BMC_HPP
#define GIA_BMC_HPP

#include <vector>
#include <iostream>
#include <utility>
#include <cassert>

#define LIN64
#include <base/main/main.h>
#include <aig/aig/aig.h>
#include <aig/gia/gia.h>
#include <misc/util/abc_global.h>
#include <ext-libs/abc/abc_api.hpp>


namespace axekit {


// BMC3 routine : returns <SAT/UNSAT, num_frames to SAT>
// BMC returns true if atleast one of the output is asserted.
// Does not track in case of multiple outputs, if some are SAT or UNSAT.
// this version checks strictly for max_frames.
// Cannot distinguish from other cases.
// Cannot use the cex generated further.
std::pair <bool, int> bmc ( abc::Abc_Ntk_t *pNtk, const int max_frames = 10000,
			    const bool verify_cex = false );

std::pair <bool, int> bmc ( abc::Gia_Man_t *gia, const int max_frames = 10000,
			    const bool verify_cex = false );

}

#endif


//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
