// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
/**
 * @file mc_make_miter.hpp
 *
 * @brief 
 *
 * @author Arun <arun@informatik.uni-bremen.de>
 */

#ifndef MC_MAKE_MITER_HPP
#define MC_MAKE_MITER_HPP

#include <iostream>
#include <fstream>
#include <functions/maniac/mc_design.hpp>
#include <utils/common_utils.hpp>
#include <cassert>

namespace maniac
{
// TOOD give meaningful names.
enum class mc_miter_type {type1, type2, type3, type4, type5_0, type5_1, type5_2};

void mc_make_miter( const std::string &out_filename,
		    const mc_design &golden, const mc_design &approx,
		    std::string miter_module_name, const mc_port &port_to_check,
		    const unsigned long &max_error, const unsigned signed_outputs,
		    const mc_miter_type &miter_type );

void mc_make_miter( const std::string &out_filename,
		    const mc_design &golden, const mc_design &approx,
		    std::string miter_module_name, const mc_port &port_to_check,
		    const unsigned long &max_error, const unsigned signed_outputs,
		    const unsigned long &clk_cycles, const mc_miter_type &miter_type );


}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
